package com.training.hackathon.service;

import com.training.hackathon.model.Asset;
import com.training.hackathon.repository.AssetRepository;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Component
public class AssetService {

    @Autowired
    private AssetRepository assetRepository;

    public List<Asset> findAll() { return assetRepository.findAll();}

    public Optional<Asset> findById(String id) { return assetRepository.findById(id); }

    public Asset addAsset(Asset asset) { return assetRepository.save(asset);}

    public void delete(String id) {
        assetRepository.deleteById(id);
    }

    public void deleteAll() {
        assetRepository.deleteAll();
    }

    public List<Asset> findBySymbol(String symbol) {
        List<Asset> symbolList = new ArrayList<>();

        List<Asset> allAssets = assetRepository.findAll();

        for(Asset asset : allAssets) {
            if (asset.getSymbol().equals(symbol)) {
                symbolList.add(asset);
            }
        }

        return symbolList;

    }

    public List<Asset> findByCurrency(String currency) {
        List<Asset> assetList = new ArrayList<>();

        List<Asset> allAssets = assetRepository.findAll();

        for(Asset asset : allAssets) {
            if (asset.getCurrency().equals(currency)) {
                assetList.add(asset);
            }
        }

        return assetList;

    }

    public List<Asset> findByExchange(String exchange) {
        List<Asset> assetList = new ArrayList<>();

        List<Asset> allAssets = assetRepository.findAll();

        for(Asset asset : allAssets) {
            if (asset.getExchange().equals(exchange)) {
                assetList.add(asset);
            }
        }

        return assetList;
    }

    public List<Asset> findByDate(Date date) {
        List<Asset> assetList = new ArrayList<>();

        List<Asset> allAssets = assetRepository.findAll();
        for(Asset asset : allAssets) {
            if (DateUtils.isSameDay(asset.getDate(), date)) {
                assetList.add(asset);
            }
        }

        return assetList;
    }

    public void updateAssetById(Asset newAsset, String id) {

            assetRepository.findById(id)
                    .map(asset -> {
                        asset.setSymbol(newAsset.getSymbol());
                        asset.setLast(newAsset.getLast());
                        asset.setVolume(newAsset.getVolume());
                        asset.setCurrency(newAsset.getCurrency());
                        asset.setExchange(newAsset.getExchange());
                        asset.setBid(newAsset.getBid());
                        asset.setAsk(newAsset.getAsk());
                        asset.setOpen(newAsset.getOpen());
                        asset.setClose(newAsset.getClose());
                        asset.setLow(newAsset.getLow());
                        asset.setHigh(newAsset.getHigh());
                        asset.setQuantity(newAsset.getQuantity());
                        return assetRepository.save(asset);
                    });

        }

    public List<Asset> filterByRange (String field, float min, float max) {
        switch (field){
            case "last":
                return assetRepository.findByLastRange(min,max);
            case "volume":
                return assetRepository.findByVolumeRange(min,max);
            case "bid":
                return assetRepository.findByBidRange(min,max);
            case "ask":
                return assetRepository.findByAskRange(min,max);
            case "open":
                return assetRepository.findByOpenRange(min,max);
            case "close":
                return assetRepository.findByCloseRange(min,max);
            case "low":
                return assetRepository.findByLowRange(min,max);
            case "high":
                return assetRepository.findByHighRange(min,max);
            case "quantity":
                return assetRepository.findByQuantityRange(min,max);
            default:
                return null;
        }
    }
}

