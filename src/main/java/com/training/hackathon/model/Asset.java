package com.training.hackathon.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Getter
@Setter
@Document
public class Asset {

    @Id
    private String id;
    private String symbol;
    private double last;
    private int volume;
    private String currency;
    private String exchange;
    private double bid;
    private double ask;
    private double open;
    private double close;
    private double low;
    private double high;
    private int quantity;
    @JsonFormat(pattern="MM-dd-yyyy")
    private Date date;

}
