package com.training.hackathon.controller;

import com.training.hackathon.model.Asset;
import com.training.hackathon.service.AssetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/assets")
public class AssetController {
    @Autowired
    private AssetService assetService;

    @GetMapping
    public List<Asset> findAll() { return assetService.findAll(); }

    @GetMapping("/id/{id}")
    public ResponseEntity<Asset> findById(@PathVariable String id) {
        try {
            return new ResponseEntity<>(assetService.findById(id).get(), HttpStatus.OK);
        } catch (NoSuchElementException ex) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/symbol/{symbol}")
    public List<Asset> findBySymbol(@PathVariable String symbol) {
        return assetService.findBySymbol(symbol);
    }

    @GetMapping("/currency/{currency}")
    public List<Asset> findByCurrency (@PathVariable String currency) {
        return assetService.findByCurrency(currency);
    }

    @GetMapping("/exchange/{exchange}")
    public List<Asset> findByExchange(@PathVariable String exchange) {
        return assetService.findByExchange(exchange);
    }

    @GetMapping("/date/{date}")
    public List<Asset> findByDate(@PathVariable @DateTimeFormat (pattern = "MM-dd-yyyy") Date date) {
        return assetService.findByDate(date);
    }

    @DeleteMapping("{id}")
    public ResponseEntity delete(@PathVariable String id) {
        assetService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @DeleteMapping()
    public ResponseEntity deleteAll() {
        assetService.deleteAll();
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PostMapping
    public Asset addAsset(@RequestBody Asset asset) { return assetService.addAsset(asset); }

    @PutMapping("{id}")
    public ResponseEntity updateAssetById(@RequestBody Asset newAsset, @PathVariable String id) {
       try {
           assetService.updateAssetById(newAsset, id);
           return new ResponseEntity(HttpStatus.OK);
       }
       catch (NoSuchElementException ex) {
           return new ResponseEntity(HttpStatus.NOT_FOUND);
       }
    }


    @GetMapping("/filter/{field}/{min}/{max}")
    public List<Asset> filter(@PathVariable String field, @PathVariable float min, @PathVariable float max){
        return assetService.filterByRange(field, min, max);


    }






}
