package com.training.hackathon.repository;

import com.training.hackathon.model.Asset;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface AssetRepository extends MongoRepository<Asset, String> {

    @Query("{last :{$gte: ?0, $lte: ?1}}")
    List<Asset> findByLastRange(float min, float max);

    @Query("{volume :{$gte: ?0, $lte: ?1}}")
    List<Asset> findByVolumeRange(float min, float max);

    @Query("{bid :{$gte: ?0, $lte: ?1}}")
    List<Asset> findByBidRange(float min, float max);

    @Query("{ask :{$gte: ?0, $lte: ?1}}")
    List<Asset> findByAskRange(float min, float max);

    @Query("{open :{$gte: ?0, $lte: ?1}}")
    List<Asset> findByOpenRange(float min, float max);

    @Query("{close :{$gte: ?0, $lte: ?1}}")
    List<Asset> findByCloseRange(float min, float max);

    @Query("{low :{$gte: ?0, $lte: ?1}}")
    List<Asset> findByLowRange(float min, float max);

    @Query("{high :{$gte: ?0, $lte: ?1}}")
    List<Asset> findByHighRange(float min, float max);

    @Query("{quantity :{$gte: ?0, $lte: ?1}}")
    List<Asset> findByQuantityRange(float min, float max);

}
