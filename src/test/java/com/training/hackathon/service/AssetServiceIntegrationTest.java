package com.training.hackathon.service;


import com.training.hackathon.model.Asset;
import com.training.hackathon.repository.AssetRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@SpringBootTest
public class AssetServiceIntegrationTest {
//
//    @Autowired
//    private AssetService assetService;
//
//    @Autowired
//    private AssetRepository assetRepository;

//    @Test
//    public void testAssetServiceUpdateAssetById(){
//        Asset testAsset = new Asset();
//        Asset newAsset = new Asset();
//
//        testAsset.setId("1");
//        testAsset.setSymbol("APPL");
//        testAsset.setLast(136.3);
//        testAsset.setVolume(121);
//        testAsset.setCurrency("USD");
//        testAsset.setExchange("NYSE");
//        testAsset.setBid(122.14);
//        testAsset.setAsk(121.15);
//        testAsset.setOpen(113.1);
//        testAsset.setClose(144.2);
//        testAsset.setLow(111.1);
//        testAsset.setHigh(166.2);
//        testAsset.setQuantity(23);
//        assetService.addAsset(testAsset);
//
//        newAsset.setId("1");
//        newAsset.setSymbol("APPL");
//        newAsset.setLast(188.8);
//        newAsset.setVolume(333);
//        newAsset.setCurrency("USD");
//        newAsset.setExchange("NYSE");
//        newAsset.setBid(111.14);
//        newAsset.setAsk(121.15);
//        newAsset.setOpen(113.1);
//        newAsset.setClose(144.2);
//        newAsset.setLow(111.1);
//        newAsset.setHigh(166.2);
//        newAsset.setQuantity(23);
//
//        assetService.updateAssetById(newAsset,"1");
//        assertEquals(188.8, assetRepository.findById("1").get().getLast());
//        assertEquals(333, assetRepository.findById("1").get().getVolume());
//        assertEquals(111.14, assetRepository.findById("1").get().getBid());
//    }





}
