package com.training.hackathon.service;

import com.training.hackathon.repository.AssetRepository;
import com.training.hackathon.model.Asset;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest
public class AssetServiceTest {

    @Autowired
    private AssetService assetService;

    @MockBean
    private AssetRepository assetRepository;

    @Test
    public void assetOneServiceDeleteAll() {
        List<Asset> allAssets = new ArrayList<>();
        Asset assetOne = new Asset();
        Asset assetTwo = new Asset();
        assetOne.setId("1234");
        assetTwo.setId("2345");
        allAssets.add(assetOne);
        allAssets.add(assetTwo);

        when(assetRepository.findAll()).thenReturn(allAssets);

        assetService.deleteAll();

        verify(assetRepository).deleteAll();
    }

    @Test
    public void assetOneServiceFindByExchange() {
        List<Asset> allAssets = new ArrayList<>();
        Asset assetOne = new Asset();
        Asset assetOne2 = new Asset();
        assetOne.setId("1234");
        assetOne.setExchange("LSE");
        assetOne2.setId("2345");
        assetOne2.setExchange("NYSE");
        allAssets.add(assetOne);
        allAssets.add(assetOne2);

        List<Asset> lseAssets = new ArrayList<>();
        for(Asset asset : allAssets) {
            if (asset.getExchange().equals("LSE")) {
                lseAssets.add(asset);
            }
        }

        when(assetRepository.findAll()).thenReturn(allAssets);

        assertEquals(lseAssets, assetService.findByExchange("LSE"));

    }

    @Test
    public void assetOneServiceFindBySymbol() {
        List<Asset> allAssets = new ArrayList<>();
        Asset assetOne = new Asset();
        Asset assetOne2 = new Asset();
        assetOne.setId("1234");
        assetOne.setSymbol("NASDAQ:AAPL");
        assetOne2.setId("2345");
        assetOne2.setSymbol("NASDAQ:MSFT");
        allAssets.add(assetOne);
        allAssets.add(assetOne2);

        List<Asset> nasdaqmsftAssets = new ArrayList<>();
        for(Asset asset : allAssets) {
            if (asset.getSymbol().equals("NASDAQ:MSFT")) {
                nasdaqmsftAssets.add(asset);
            }
        }

        when(assetRepository.findAll()).thenReturn(allAssets);

        assertEquals(nasdaqmsftAssets, assetService.findBySymbol("NASDAQ:MSFT"));
        assertEquals(new ArrayList<>(), assetService.findBySymbol("NYSE:A"));
    }

    @Test
    public void assetOneServiceFindByCurrency() {
        List<Asset> allAssets = new ArrayList<>();
        Asset assetOne = new Asset();
        Asset assetOne2 = new Asset();
        assetOne.setId("1234");
        assetOne.setCurrency("YEN");
        assetOne2.setId("2345");
        assetOne2.setCurrency("CAD");
        allAssets.add(assetOne);
        allAssets.add(assetOne2);

        List<Asset> cadAssets = new ArrayList<>();
        for(Asset asset : allAssets) {
            if (asset.getCurrency().equals("CAD")) {
                cadAssets.add(asset);
            }
        }

        when(assetRepository.findAll()).thenReturn(allAssets);

        assertEquals(cadAssets, assetService.findByCurrency("CAD"));
    }



    @Test
    public void assetOneServiceDelete() {
        List<Asset> allAssets = new ArrayList<>();
        Asset assetOne = new Asset();
        Asset assetTwo = new Asset();
        assetOne.setId("11");
        assetTwo.setId("12");
        allAssets.add(assetOne);
        allAssets.add(assetTwo);

        when(assetRepository.findAll()).thenReturn(allAssets);

        assetService.delete("11");

        verify(assetRepository).deleteById("11");
    }


//    @Test
//    public void assetOneServiceFilterByRange() {
//        Asset assetOne = new Asset();
//        Asset assetTwo = new Asset();
//
//        assetOne.setId("12");
//        assetOne.setSymbol("APPL");
//        assetOne.setLast(136.3);
//
//        assetTwo.setId("34");
//        assetTwo.setSymbol("APPL");
//        assetTwo.setLast(188.8);
//
//
//        List<Asset> allAssets = new ArrayList<>();
//        List<Asset> targetAssets = new ArrayList<>();
//        allAssets.add(assetOne);
//        allAssets.add(assetTwo);
//
//        targetAssets.add(assetOne);
//
//        when(assetRepository.findAll()).thenReturn(allAssets);
//
//        assertEquals(targetAssets, assetService.filterByRange("last", 120.0F,140.0F));
//    }
}
