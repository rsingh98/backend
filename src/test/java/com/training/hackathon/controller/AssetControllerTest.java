package com.training.hackathon.controller;

import com.training.hackathon.model.Asset;
import com.training.hackathon.service.AssetService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.containsString;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@WebMvcTest(AssetController.class)
public class AssetControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AssetService assetService;

    @Test
    public void testAssetControllerFindAll() throws Exception {
        List<Asset> allAssets = new ArrayList<Asset>();
        Asset testAsset = new Asset();
        testAsset.setId("1");
        allAssets.add(testAsset);

        when(assetService.findAll()).thenReturn(allAssets);

        this.mockMvc.perform(get("/api/v1/assets"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().string(containsString("\"id\":\"1\"")));
    }


//    @Test
//    public void testAssetControllerAddAsset() throws Exception {
//        List<Asset> allAssets = new ArrayList<Asset>();
//        Asset testAsset = new Asset();
//        testAsset.setId("2");
//        allAssets.add(testAsset);
//
//        when(assetService.addAsset(testAsset)).thenReturn(testAsset);
//
//        this.mockMvc.perform(get("/api/v1/assets"))
//                .andDo(print())
//                .andExpect(status().isOk())
//                .andExpect(MockMvcResultMatchers.content().string(containsString("\"id\":\"2\"")));
//    }

    public void testAssetControllerDeleteAll() throws Exception {
        List<Asset> allAssets = new ArrayList<>();
        Asset assetOne = new Asset();
        Asset assetTwo = new Asset();
        assetOne.setId("1234");
        assetTwo.setId("2345");
        allAssets.add(assetOne);
        allAssets.add(assetTwo);

        when(assetService.findAll()).thenReturn(allAssets);

        this.mockMvc.perform(delete("/api/v1/assets"))
                .andDo(print())
                .andExpect(status().isNoContent());

    }

//    // not working yet might have to change some things in controller
//    @Test
//    public void testAssetControllerFindByExchange() throws Exception {
//        List<Asset> allAssets = new ArrayList<>();
//        Asset testAsset = new Asset();
//        Asset testAsset2 = new Asset();
//        testAsset.setId("1234");
//        testAsset.setExchange("LSE");
//        testAsset2.setId("2345");
//        testAsset2.setExchange("NYSE");
//        allAssets.add(testAsset);
//        allAssets.add(testAsset2);
//
//        List<Asset> lseAssets = new ArrayList<>();
//        for(Asset asset : allAssets) {
//            if (asset.getExchange().equals("LSE")) {
//                lseAssets.add(asset);
//            }
//        }
//
//        when(assetService.findByExchange("LSE")).thenReturn(lseAssets);
//
//        this.mockMvc.perform(get("/api/v1/assets/{exchange}", "LSE"))
//                .andDo(print())
//                .andExpect(status().isOk())
//                .andExpect(MockMvcResultMatchers.content().string(containsString("\"exchange\":\"LSE\"")));
//
//    }

    @Test
    public void testAssetControllerFindBySymbol() {

    }

    @Test
    public void testAssetControllerFindByCurrency() {

    }
}
